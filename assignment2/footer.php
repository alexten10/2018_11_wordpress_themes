    <footer class="main col-xs-12">
    
      <nav id="footer">
        <img id="footer_dog_img" src="<?php echo get_template_directory_uri(); ?>/images/Playful_dog.svg" alt="" height="80">
        <?php wp_nav_menu(['menu' => 'footer_assign2_1']); ?>
        <?php wp_nav_menu(['menu' => 'footer_assign2_2']); ?>
        <img id="footer_logo_img" src="<?php echo get_template_directory_uri(); ?>/images/Wordmarks.svg" alt="" height="30">
      </nav>

        <p class="copyright">Contents copyright (c) 2010-2014 by Westlands Partners</p>
    </footer>
    
  </div><!-- /container -->
  
  <?php wp_footer(); ?>
  </body>
</html>