 
 
 
 
 
     <!--bottom line image links for home page -->
      <div id="secondary" class="col-xs-12">
        
        <?php $posts = get_posts(['category_name' => 'features', 'numberposts' => 3]); ?><!--get 3 latest posts from category 'features'-->
       
        <?php foreach($posts as $post) : ?>
          <div class="callout col-sm-4 col-xs-12">
            <div class="col-xs-12">
              <a href="<?php echo get_the_permalink(); ?>"><!-- get link to current post-->
                <?php the_post_thumbnail('thumbnail'); ?><!--show post's thumbnail image in size 'thumbnail' 150*150-->
              </a>
              <div class="featured_title"><?php the_title('<p>', '</p>'); ?></div>
              <p><?php echo get_the_excerpt($post->ID);?></p>
              <span><a class="button_read_more" href="<?php the_permalink(); ?>">Read more &gt;</a></span>
            </div>
          </div><!-- /callout -->
        <?php endforeach; ?>

      </div><!-- /secondary -->