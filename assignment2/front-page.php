<?php get_header(); ?>
<!-- front-page.php is for home page -->


    <div id="content" class="col-xs-12">

      <!-- big image on home page -->
      <div id="primary" class="col-xs-12">
        <div id="featured" class="col-xs-12">
          <?php echo do_shortcode('[metaslider id="123"]'); ?>
        </div><!-- /featured -->
      </div><!-- /primary -->

      <?php get_sidebar('home'); ?>

    </div><!-- /content -->



<?php get_footer(); ?>

