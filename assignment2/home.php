<?php get_header(); ?>
<!-- home.php is for blog posts index page -->


    <div id="content" class="col-xs-12">
      
      <div id="primary" class="col-xs-12 col-sm-9">
        
        <?php while(have_posts()) : ?>
          <?php the_post(); ?> <!-- $post() is an incrementer -->
          
          <?php if ( in_category( 'dog_food' ) || in_category( 'cat_food' ) ) : ?><!-- show only posts from category 'products'-->
            <a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a>    <!--display title, 1 - before, 2 -after -->
            
            <p><?php the_date(); ?></p>
            
            <?php the_post_thumbnail('thumbnail'); ?><!-- display image in size 'thumbnail'-->
            
            <?php the_excerpt('<div>', '</div>'); ?>
            
            <p><a href="<?php the_permalink(); ?>">READ POST...</a></p>
            <hr />
          <?php endif; ?>
          
        <?php endwhile; ?>
        
        
        
        
        <!-- ***************************************************************************
          ALTERNATIVE CODE. To show only 3 latest posts from TWO categories 'dog_food' and 'cat_food'
          
          
        <?php $i=1; while($i<4 && have_posts()) : ?>
          <?php the_post(); ?>
          
          <?php if ( in_category( 'dog_food' ) || in_category( 'cat_food' ) ) : ?>
            <a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a>
            <p><?php the_date(); ?></p>
            <?php the_post_thumbnail('thumbnail'); ?>
            <?php the_excerpt('<div>', '</div>'); ?>
            <p><a href="<?php the_permalink(); ?>">READ POST...</a></p>
            <hr />
            <?php $i++ ?>
          <?php endif; ?>
          
        <?php endwhile; ?>
        
        
        ************************************************************************************-->
        
        
        
        
        
        
        <!-- ***************************************************************************
          ALTERNATIVE CODE. Take only 3 latst posts from ALL categories
          and among these 3 posts, show only those that belong to categories 'dog_food' and 'cat_food'
          
          
        <?php $posts = get_posts(['numberposts' => 3]); ?>
        
        <?php while(have_posts()) : ?>
          <?php the_post(); ?>
          
          <?php if ( in_category( 'dog_food' ) || in_category( 'cat_food' ) ) : ?>
            <a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a>
            <p><?php the_date(); ?></p>
            <?php the_post_thumbnail('thumbnail'); ?>
            <?php the_excerpt('<div>', '</div>'); ?>
            <p><a href="<?php the_permalink(); ?>">READ POST...</a></p>
            <hr />
          <?php endif; ?>
          
        <?php endwhile; ?>
        
        
        ************************************************************************************-->
        
        
        
      </div><!-- /primary -->
      
      <?php get_sidebar(); ?>
      
    </div><!-- /content -->



<?php get_footer(); ?>

