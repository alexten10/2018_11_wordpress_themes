<?php get_header(); ?>

    <div id="content" class="col-xs-12">
      
      <div id="primary" class="col-xs-12 col-sm-9">
        <?php while(have_posts()) : ?>
          <?php the_post(); ?> <!-- $post() is an incrementer -->
          
          <?php the_title('<h1>', '</h1>'); ?>    <!--display title, 1 - before, 2 -after -->
          
          <p><?php the_date(); ?></p>
          
          <?php the_post_thumbnail('thumbnail'); ?><!-- display image in size 'thumbnail'-->
          
          <?php the_content('<div>', '</div>'); ?>
          
        <?php endwhile; ?>
      </div><!-- /primary -->
      
      <?php get_sidebar(); ?>
      
    </div><!-- /content -->



<?php get_footer(); ?>

