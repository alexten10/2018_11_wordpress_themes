<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>home</title>
  
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

<?php wp_head(); ?>
</head>



<body class="page-template-default">
  
  <!--utility navigation, outside of the container -->
  <div id="util_nav">
    <nav id="util">
      <a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/fbook_icon.svg" alt="" width="20"></a>
      <a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/twitr_icon.svg" alt="" width="20"></a>
      <?php wp_nav_menu(['menu' => 'utility_assign2']); ?>
    </nav>
  </div>
       
  
  <div class="container">
    
    
    <div class="row">
      <header class="main col-xs-12">
        <img class="img_head" src="<?php echo get_template_directory_uri() ?>/images/header.jpg" alt="" >
        <a class="menu_toggle" href="#">Menu</a>
      </header>
    </div><!-- row-->
    
    
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main_assign2']); ?>
    </nav>