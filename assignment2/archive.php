<?php get_header(); ?>
<!-- home.php is for blog posts index page -->


    <div id="content" class="col-xs-12">
      
      <div id="primary" class="col-xs-12 col-sm-9">
        
        <h1><?php the_archive_title() ?></h1>
        <?php while(have_posts()) : ?>
          <?php the_post(); ?> <!-- $post() is an incrementer -->
          
            <a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a>    <!--display title, 1 - before, 2 -after -->
            
            <p><?php the_date(); ?></p>
            
            <?php the_post_thumbnail('thumbnail'); ?><!-- display image in size 'thumbnail'-->
            
            <?php the_excerpt('<div>', '</div>'); ?>
            
            <p><a href="<?php the_permalink(); ?>">READ POST...</a></p>
            <hr />
          
        <?php endwhile; ?>
      </div><!-- /primary -->
      
      <?php get_sidebar(); ?>
      
    </div><!-- /content -->



<?php get_footer(); ?>

