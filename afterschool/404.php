<!-- this file is for 404 errors -->
<?php get_header(); ?>

<body class="page-template-default">
  <div class="container">

    <header class="main col-xs-12">
      <span class="site_title">After School</span>
      
      <!--utility menu-->
      <nav id="util">
        <ul class="menu">
          <?php wp_nav_menu(['menu' => 'utility']);?><!-- load menu called 'utility'-->
        </ul>
      </nav>
      
      <a class="menu_toggle" href="#">Menu</a>
    </header>

    <!--main menu-->
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main']);?><!-- load menu called 'main'-->
    </nav>



    <div id="content" class="col-xs-12">

      <!-- left column with all posts listing -->
      <div id="primary" class="col-xs-12 col-sm-9">
        <h1 class="archive_title">404 Error Page Not Found</h1>
      </div><!-- /primary -->

      <!-- right column with posts' images as links-->
      <div id="secondary" class="col-xs-12 col-sm-3">
        <?php $posts = get_posts(['category_name' => 'features']); ?><!--get all posts from category 'features'-->
        <?php foreach($posts as $post) : ?>
          <div class="callout col-xs-12">
            <div class="col-xs-12">
              <a href="<?php echo get_the_permalink(); ?>"><!-- get link to current post-->
                <?php echo get_the_post_thumbnail(); ?><!--show post's thumbnail image-->
              </a>
              <div class="caption  col-xs-12"><!--post's title text over the image-->
                  <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
              </div><!-- /caption -->
            </div>
          </div><!-- /callout -->
        <?php endforeach; ?>
      </div><!-- /secondary -->

    </div><!-- /content -->



  <script>

      $(document).ready(function(){
        setMenu();
        $(window).resize(function(){
          setMenu();
        });
      });

      function setMenu() {
        if($(window).width() < 768) {
          $('nav#main').hide();
        } else {
          $('nav#main').show();
        }
      }

    $('a.menu_toggle').click(function(){
      $('nav#main').toggle();
    });

  </script>


<?php get_footer(); ?>












