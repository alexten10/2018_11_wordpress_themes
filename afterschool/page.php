<?php get_header(); ?>

<body class="page-template-default">
  <div class="container">

    <header class="main col-xs-12">
      <span class="site_title">After School</span>
      
      <!--utility menu-->
      <nav id="util">
        <ul class="menu">
          <?php wp_nav_menu(['menu' => 'utility']);?><!-- load menu called 'utility'-->
        </ul>
      </nav>
      
      <a class="menu_toggle" href="#">Menu</a>
    </header>

    <!--main menu-->
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main']);?><!-- load menu called 'main'-->
    </nav>



    <div id="content" class="col-xs-12">

      <!-- left column with page content -->
      <div id="primary" class="col-xs-12 col-sm-9">
        <?php while(have_posts()) : ?>
          
          <?php the_post(); ?> <!-- $post() is an incrementer -->
          
          <?php the_title('<h1>', '</h1>'); ?> <!--display title, 1 - before, 2 -after -->
          
          <!--2 VARIANTS to display page's featured image and assign it class="alignleft"-->
          <!-- VARIANT #1 -->
          <?php the_post_thumbnail('post-thumbnail', ['class' => 'alignleft']); ?>
          <!-- VARIANT #2-->
          <!--<img src="<?php echo get_the_post_thumbnail_url();?>" class="alignleft" alt="" />-->
          
          <?php the_content('<p>', '</p>'); ?>
          
        <?php endwhile; ?>
      </div><!-- /primary -->

      <?php get_sidebar(); ?>

    </div><!-- /content -->



<?php get_footer(); ?>












