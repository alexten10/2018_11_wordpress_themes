<?php get_header(); ?>

<body class="page-template-default">
  <div class="container">

    <header class="main col-xs-12">
      <span class="site_title">After School</span>
      
      <!--utility menu-->
      <nav id="util">
        <ul class="menu">
          <?php wp_nav_menu(['menu' => 'utility']);?><!-- load menu called 'utility'-->
        </ul>
      </nav>
      
      <a class="menu_toggle" href="#">Menu</a>
    </header>

    <!--main menu-->
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main']);?><!-- load menu called 'main'-->
    </nav>



    <div id="content" class="col-xs-12">

      <!-- left column with all posts listing -->
      <div id="primary" class="col-xs-12 col-sm-9">
        <h1 class="archive_title">You looked for: <?php echo get_search_query(); ?></h1>
        <ul>
          <?php while(have_posts()) : ?>
            <?php the_post(); ?> 
            <a href="<?php the_permalink(); ?>"><?php the_title('<li>', '</li>'); ?></a>
          <?php endwhile; ?>
        </ul>
      </div><!-- /primary -->

      <?php get_sidebar(); ?>

    </div><!-- /content -->



<?php get_footer(); ?>












