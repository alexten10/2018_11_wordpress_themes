<?php get_header(); ?>

<body class="page-template-default">
  <div class="container">

    <header class="main col-xs-12">
      <span class="site_title">After School</span>
      
      <!--utility menu-->
      <nav id="util">
        <ul class="menu">
          <?php wp_nav_menu(['menu' => 'utility']);?><!-- load menu called 'utility'-->
        </ul>
      </nav>
      
      <a class="menu_toggle" href="#">Menu</a>
    </header>

    <!--main menu-->
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main']);?><!-- load menu called 'main'-->
    </nav>



    <div id="content" class="col-xs-12">

      <!-- left column with all posts listing -->
      <div id="primary" class="col-xs-12 col-sm-9">
        <h1 class="archive_title">News</h1>
        <?php while(have_posts()) : ?>
          <?php the_post(); ?> <!-- $post() is an incrementer -->
          <a href="<?php the_permalink() ?>"><?php the_title('<h2>', '</h2>'); ?></a>    <!--display title, 1 - before, 2 -after -->
          <p><?php the_date(); ?></p>
          <?php the_excerpt('<div>', '</div>'); ?>
        <?php endwhile; ?>
      </div><!-- /primary -->

      <?php get_sidebar(); ?>

    </div><!-- /content -->



<?php get_footer(); ?>












