      <!--bottom line image links for home page -->
      <div id="secondary" class="col-xs-12">
        
        <?php $posts = get_posts(['category_name' => 'features', 'numberposts' => 3]); ?><!--get 3 latest posts from category 'features'-->
        
        <?php foreach($posts as $post) : ?>
          <div class="callout col-sm-4 col-xs-12">
            <div class="col-xs-12">
              <a href="<?php echo get_the_permalink(); ?>"><!-- get link to current post-->
                <?php echo get_the_post_thumbnail(); ?><!--show post's thumbnail image-->
              </a>
              <div class="caption  col-xs-12">
                <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
              </div><!-- /caption -->
            </div>
          </div><!-- /callout -->
        <?php endforeach; ?>

      </div><!-- /secondary -->