<?php
//Template Name: Home
 get_header(); ?>

<body class="home">
  <div class="container">

    <header class="main col-xs-12">
      <span class="site_title">After School</span>
      
      <!--utility menu-->
      <nav id="util">
        <ul class="menu">
          <?php wp_nav_menu(['menu' => 'utility']);?><!-- load menu called 'utility'-->
        </ul>
      </nav>
      
      <a class="menu_toggle" href="#">Menu</a>
    </header>

    <!--main menu-->
    <nav id="main" class="col-xs-12">
      <?php wp_nav_menu(['menu' => 'main']);?><!-- load menu called 'main'-->
    </nav>



    <div id="content" class="col-xs-12">

      <!-- big image on home page -->
      <div id="primary" class="col-xs-12">
        <div id="featured" class="col-xs-12">
          
          
          <?php $posts = get_posts(['category_name' => 'slides', 'numberposts' => 1]); ?><!--get 1 latest post from category 'slides'-->
          <?php foreach($posts as $post) : ?>
            <?php echo get_the_post_thumbnail(); ?>
          <?php endforeach; ?>
        
          <div class="caption col-xs-12">
            <a href="<?php echo get_the_permalink($post->ID); ?>">Find out about our programs for children</a>
          </div><!-- /caption -->
        </div><!-- /featured -->
      </div><!-- /primary -->

      <?php get_sidebar('home'); ?>

    </div><!-- /content -->



<?php get_footer(); ?>























