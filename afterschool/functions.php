<?php
if ( function_exists('register_nav_menus') ) {
 register_nav_menus();
}


if ( function_exists('add_theme_support') ) {
  add_theme_support('post-thumbnails');
  add_image_size('list-thumb', 75, 75);
}





if ( !function_exists('my_load_styles') ) {
  function my_load_styles()
  {
    //load bootstrap.css first, then next line
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', [], false, 'all');
    wp_enqueue_script('ajax', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js');
    wp_enqueue_style('myblog', get_stylesheet_uri(), ['bootstrap'], false, 'all'); //load myblog.css
    wp_enqueue_script('bootstrapjs', 'https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', ['jquery'], '4.1.3', true);
    wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', ['jquery'], '1.1', true);
  }
  
  add_action('wp_enqueue_scripts', 'my_load_styles');
}








