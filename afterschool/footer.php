    <footer class="main col-xs-12">
      <nav id="footer">
        <ul class="menu">
          <?php wp_nav_menu();?>
        </ul>
      </nav>
      <p class="copyright">Copyright &copy; 2015 by After School</p>
    </footer>

  </div><!-- /container -->

  <?php wp_footer(); ?>
  </body>
</html>