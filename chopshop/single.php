<!-- this file is for single post, not page -->
  
<?php get_header(); ?>
  
  <div class="container">
    
    <div id="image">
      <img id="banner" src="<?php echo get_template_directory_uri() ?>/images/car.jpg" alt="car" />
      <div id="site_title"><?php echo bloginfo('name'); ?></div>
    </div>
    
    <?php get_sidebar(); ?>
    
    
    <div class="main">
      
      <?php while(have_posts()) : ?>
        <?php the_post(); ?>
        
        <?php the_title('<h1>', '</h1>'); ?>
        
        <p><small>Posted on <?php the_date(); ?> at <?php the_time(); ?> by <?php the_author(); ?></small></p>
        
        <?php the_content('<div>', '</div>'); ?>
        
        <?php comments_template(); ?>
        
      <?php endwhile; ?>
      
      
    </div><!-- end main-->
    
    
    
    
  </div><!--end container -->
  
<?php get_footer(); ?>








