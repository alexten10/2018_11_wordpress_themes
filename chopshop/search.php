<!-- this file is for search results -->
<?php get_header(); ?>
  
  <div class="container">
    
    <div id="image">
      <img id="banner" src="<?php echo get_template_directory_uri() ?>/images/car.jpg" alt="car" />
      <div id="site_title"><?php echo bloginfo('name'); ?></div>
    </div>
    
    <?php get_sidebar(); ?><!--load sidebar-page.php-->
    
    
    <div class="main">
      <h1>You looked for: <?php echo get_search_query(); ?></h1>
      <!--  <h1>You searched for:</h1> -->
      
      <ul>
        <?php while(have_posts()) : ?>
          <?php the_post(); ?> 
          <a href="<?php the_permalink(); ?>"><?php the_title('<li>', '</li>'); ?></a>
        <?php endwhile; ?>
      </ul>
      
    </div><!-- end main-->
    
    
  </div><!--end container -->
  
  
<?php get_footer(); ?>