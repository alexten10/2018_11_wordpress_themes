    <div class="sidebar">
      
      <h2>sidebar</h2>
      
      <?php get_search_form(); ?>
      
<!--       <h3>search</h3>
      <form action="/" method="get">
        <input type="text" name="s" />
        <input type="submit" value="search" />
      </form> -->
      
      
      <h3>menu</h3>
      <?php wp_nav_menu(['menu' => 'sidebar']); ?>
      
      
      <h3>archives</h3>
      <ul>
        <?php wp_get_archives(); ?>
      </ul>
      
      
      <h3>categories</h3>
      <ul>
        <?php wp_list_categories(['title_li' => '']); ?>
      </ul>
    </div><!--end sidebar-->