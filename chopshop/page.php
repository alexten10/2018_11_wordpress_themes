<!-- this file is for single page, not post -->

<?php get_header(); ?>
  
  <div class="container page">
    
    <div id="image">
      <img id="banner" src="<?php echo get_template_directory_uri() ?>/images/car.jpg" alt="car" />
      <div id="site_title"><?php echo bloginfo('name'); ?></div>
    </div>
    
    <?php get_sidebar('page'); ?>
    
    
    
    <div class="main">
      
      <?php while(have_posts()) : ?>
        
        <?php the_post(); ?> 
        
        <?php the_title('<h1>', '</h1>'); ?> 
        
        <?php the_content('<div>', '</div>'); ?>
        
      <?php endwhile; ?>
      
    </div><!-- end main-->
    
    
    
  </div><!--end container -->
  
<?php get_footer(); ?>








