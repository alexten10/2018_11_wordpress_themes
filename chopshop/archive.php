<!-- this file is for archive listings -->
<?php get_header(); ?>
  
  <div class="container">
    
    <div id="image">
      <img id="banner" src="<?php echo get_template_directory_uri() ?>/images/car.jpg" alt="car" />
      <div id="site_title"><?php echo bloginfo('name'); ?></div>
    </div>
    
    <?php get_sidebar(); ?><!--load sidebar.php-->
    
    
    <div class="main">
      
      <h1><?php the_archive_title() ?></h1>
      
      <?php while(have_posts()) : ?>
        
        <?php the_post(); ?> <!-- $post() is an incrementer -->
        
        <a href="<?php the_permalink() ?>"><?php the_title('<h2>', '</h2>'); ?></a> <!--display title, 1 - before, 2 -after -->
        
        <p><small>Posted on <?php the_date(); ?> at <?php the_time(); ?> by <?php the_author(); ?></small></p>
        
        <?php the_excerpt('<div>', '</div>'); ?>
        
        <small><a href="<?php the_permalink(); ?>" >Read more...</a></small><!-- link for current post, takes only one parameter-->
        
      <?php endwhile; ?>
      
      
    </div><!-- end main-->
    
    
    
    
  </div><!--end container -->
  
<?php get_footer(); ?>








